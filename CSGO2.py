#!/usr/bin/python3 

import sys
import time
import json
from yeelight import Bulb
from yeelight import discover_bulbs
from http.server import BaseHTTPRequestHandler, HTTPServer

class Server(HTTPServer):
    def init_state(self):
        self.bulbs = Bulbs(False)
        self.flashLvl = 0
        self.Team = None


class yBulb:
    def __init__(self, ip):
        self.bulb = Bulb(ip, effect="sudden")
        properties = self.bulb.get_properties()
        self.musicMode()
        self.bulb.set_rgb(255,255,255)
        self.bulb.set_brightness(0)
        self.isOn = properties['power'] == 'on'
        self.musicOn = properties['music_on'] == 1
        self.name = properties['name']


    def turnOff(self):
        self.bulb.turn_off()

    def turnOn(self):
        self.bulb.turn_on()
        self.musicMode()
    
    def musicMode(self):
        try:
            self.bulb.start_music()
        except:
            pass

    def setrgb(self, val1, val2, val3):
        self.bulb.set_rgb(val1, val2, val3)

    def setBrightness(self, val):
        self.bulb.set_brightness(val)


class Bulbs:
    def __init__(self,lan):
        self.bulbArray = []
        if(lan):
            print("Discovering bulbs on LAN")
            bulbList = discover_bulbs()
            print("Found {} bulbs".format(len(bulbList)))
            for i in bulbList:
                self.bulbArray.append(yBulb(i['ip']))
        else:
            self.bulbArray.append(yBulb("192.168.1.100"))
            #self.bulbArray.append(yBulb("192.168.1.81"))
        self.turnOn()
        self.setColor(255,255,255)
        self.setBrightness(0)


    def turnOn(self):
        for i in self.bulbArray:
            i.turnOn()
    
    def turnOff(self):
        for i in self.bulbArray:
            i.turnOff()

    def setBrightness(self,val):
        for i in self.bulbArray:
            i.setBrightness(val)

    def setColor(self, val1, val2, val3):
        for i in self.bulbArray:
            i.setrgb(val1, val2, val3)
    def setTeamColor(self, team):
        val1 = 255
        val2 = 255
        val3 = 255 
        if(team == 'T'):
            val1 = 222
            val2 = 155
            val3 = 53
        elif team == 'CT':
            val1 = 93
            val2 = 121
            val3 = 174
        for i in self.bulbArray:
            i.setrgb(val1, val2, val3)



class MyRequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        length = int(self.headers['Content-Length'])
        body = self.rfile.read(length).decode('utf-8')

        self.parse_payload(json.loads(body))

        self.send_header('Content-type', 'text/html')
        self.send_response(200)
        self.end_headers()

    def parse_payload(self, payload):
        flashedLvl = int(self.getFlashedLvl(payload) / 255 * 100)
        if(self.getTeamColor(payload) != self.server.Team):
            self.server.bulbs.setTeamColor(self.getTeamColor(payload))
            self.server.Team = self.getTeamColor(payload)
        self.server.firstPayload = False
        if(flashedLvl != self.server.flashLvl):
            if(self.server.flashLvl == 0):
                print("flashed !")
                self.server.bulbs.setColor(255,255,255)
                self.server.bulbs.turnOn()
            self.server.flashLvl = flashedLvl
            self.server.bulbs.setBrightness(flashedLvl)
            if(flashedLvl == 0):
                self.server.bulbs.setTeamColor(self.getTeamColor(payload))

    def getTeamColor(self, payload):
        if('player') in payload:
            return payload['player']['team']

    def getFlashedLvl(self, payload):
        if('state') in payload['player']:
            return payload['player']['state']['flashed']
        else:
            return 0


    def log_message(self, format, *args):
        """
        Prevents requests from printing into the console
        """
        return

server = Server(('localhost', 3000), MyRequestHandler)
server.init_state()

try:
    server.serve_forever()
except (KeyboardInterrupt, SystemExit):
    pass